/* Show the arrays if are Equal or not..
 */
public class Exercise_01 {
    public static void main(String[] args) {
        long[] array1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        long[] array2 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        long[] array3 = {0, 2, 3, 4, 5, 6, 7, 8, 9};
        long[] array4 = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        if (isEqual(array1, array2)) {
            displayArrays("\nArrays are EQUAL !", array1, array2);
        } else {
            displayArrays("\nArrays are NOT EQUAL !", array1, array1);
        }

        if (isEqual(array3, array4)) {
            displayArrays("\nArrays are EQUAL !", array3, array4);
        } else {
            displayArrays("\nArrays are NOT EQUAL !", array3, array4);
        }
    }

    public static void displayArrays(String message, long[] arr1, long[] arr2) {
        System.out.println(message);
        displayArray(arr1);
        displayArray(arr2);
    }

    public static void displayArray(long[] array) {

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static boolean isEqual(long[] array1, long[] array2) {
        boolean equal = true;

        if (array1.length != array2.length) {
            equal = false;
        } else {
            for (int i = 0; i < array1.length; i++) {
                if (array1[i] != array2[i]) {
                    equal = false;
                }
            }
        }
        return equal;
    }
}
