// Three metods with diferent paramenters each and return TRUE or False,
// like: INT + INT => INT (123 and 1) , STRING + STING => STRING, STRING + INT => STRING
// Converting Upercase to lowercase

public class Exercise_02 {
    public static void main(String[] args) {
        System.out.println(containsString("Anna have apples!", "Anna"));
        boolean var = containsString("Anna have apples!", "John");
        System.out.println(var);
        System.out.println(containsInt("Anna have 3 apples.", 3));
        System.out.println(containsInt("Anna have 2 apples.", 3) + "\n");
        System.out.println(intcontainsInt(254, 2));
        System.out.println(intcontainsInt(291, 3) + "\n");
        System.out.println(showDigitbyDivision(100, 1200));
    }

    public static boolean containsString(String a, String b) {
        a = a.toLowerCase();
        b = b.toLowerCase();
        System.out.println("\na is: " + a + "\nb is: " + b);
         return a.contains(b);
    }

    public static boolean containsInt(String a, int b) {
        String c = String.valueOf(b);
        System.out.println();
        System.out.println("c is: " + c + "\nVerify if in " + b + " is found in the message: " + a);
        return a.contains(c);
    }

    public static boolean intcontainsInt(int a, int b) {
        String c = String.valueOf(a);
        String d = String.valueOf(b);
        System.out.println("Verify if " + b + " is found in the number: " + a);

        return c.contains(d);
    }

    public static boolean showDigitbyDivision(int a, int b) {
        int c;
        boolean notFound = false;
        do {
            c = a % 10;
            if (c == b) {
                return true;
            }        System.out.println("Divided Found = " + a + " and " + b);
            a = a/10;
        } while (a != 0);
        System.out.println("\na Value = "+ a + ", b Value = " + b + ", c Value = " +c);
        return notFound;
    }
}