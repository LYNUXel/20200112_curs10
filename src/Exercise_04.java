import java.util.Random;

public class Exercise_04 {
    // Generate array of int random numbers
    // Create an method which filter the even numbers
    // Create an method with compute average
    public static void main(String[] args) {
        int[] array = generateRandomArray(10);
        displayArray(array);
        System.out.println();
        System.out.println(average(array));
    }

    public static int[] generateRandomArray(int a) {
        int[] arr = new int[a];
        Random rand = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(100);
        }
        return arr;
    }

    public static void displayArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static double average(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        double avg = (double) sum / arr.length;
        System.out.print("Average is: ");
        return avg;
    }
}